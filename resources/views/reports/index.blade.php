@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @foreach ($reports as $report)
                <div class="card">
                    <div class="card-header">
                        <p class="float-right ml-2"><a class='remove' href="{{ route('reports.destroy', $report->id) }}">Delete</a></p>
                        <p class="float-right"><a href="{{ route('reports.edit', $report->id) }}">Edit</a></p>
                        <p class="float-left">{{ $report->title }}</p>
                    </div>

                    <div class="card-body">
                        <img src="/img/{{ $report->image }}" class="w-100 mb-5" alt="">
                        <p class="text-justify">{{ $report->text }}</p>
                    </div>
                </div>
            @endforeach
            @if (!count($reports))
                <div class="card">
                    <div class="card-body text-center">
                        No records
                    </div>
                </div>
            @endif            
            <div class="d-flex mt-5 justify-content-center">{!! $reports->links() !!}</div>
        </div>
        <script>
            document.addEventListener("DOMContentLoaded", function() {
                let token = document.querySelector("input[name='_token']").value;
                document.querySelectorAll('.remove').forEach(v => v.onclick = async function(e) {
                    e.preventDefault();

                    let result = await fetch(this.href, 
                        {
                            method: 'DELETE',
                            headers: {
                                "X-Requested-With": "XMLHttpRequest",
                                'X-CSRF-TOKEN': token
                            }
                        }).then(res => {
                            status = res.status;
                            return res;
                        }).then(res => res.json());

                    if (status != 200) {
                        alert(result.message);
                    } else {
                        alert('Запись успешно удалена');
                        location.reload();
                    }

                })
            });
        </script>
    </div>
</div>
@endsection
