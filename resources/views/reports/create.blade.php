@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="alert alert-danger d-none" role="alert">
                Не все поля заполнены
            </div>
            <form id="report" >
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" placeholder="Enter title">
                </div>
                <div class="form-group">
                    <label for="text">Text</label>
                    <textarea class="form-control" name="text" id="text" cols="30" rows="10"></textarea>
                </div>
                <div class="form-group">
                    <label for="image">Image</label>
                    <img src="" class="preview" width="100" alt="">
                    <input type="file" class="form-control-file" id="image" required>
                </div>
                <button type="button" class="submit btn btn-primary">Submit</button>
            </form>
            <script>
                document.addEventListener("DOMContentLoaded", function() {
                    let token = document.querySelector("input[name='_token']").value;
                    let alert = document.querySelector('.alert');

                    document.querySelector('#image').onchange = function() {
                        let reader = new FileReader();

                        reader.onload = function(e) {
                            document.querySelector('.preview').src = e.target.result;
                        }

                        reader.readAsDataURL(this.files[0]);
                    }
                    
                    document.querySelector('.submit').onclick = async function() {
                        let data = new FormData();
                        let error = false;
                        data.append('title', document.querySelector('#title').value);
                        data.append('text', document.querySelector('#text').value);
                        data.append('image', document.querySelector('#image').files[0] || '');
                        
                        for(let value of data) {
                            if (!value[1]) {
                                document.querySelector('#'+value[0]).classList.remove('is-valid');
                                document.querySelector('#'+value[0]).classList.add('is-invalid');
                                error = true;
                            } else {
                                document.querySelector('#'+value[0]).classList.remove('is-invalid');
                                document.querySelector('#'+value[0]).classList.add('is-valid');
                            }
                        }

                        if (error) {
                            alert.classList.remove('d-none');
                            alert.innerText = 'Не все поля заполнены';
                            return;
                        }
                        let status = 0;
                        alert.classList.add('d-none');

                        let result = await fetch('{{ route("reports.store") }}', {
                            method: 'post',
                            credentials: "same-origin",
                            body: data,
                            headers: {
                                "X-Requested-With": "XMLHttpRequest",
                                'X-CSRF-TOKEN': token
                            }
                        }).then(res => {
                            status = res.status;
                            return res;
                        }).then(res => res.json());

                        if (status != 200) {
                            alert.classList.remove('d-none');
                            alert.innerText = result.message;
                        } else {
                            location.href = "{{ route('reports.index') }}"
                        }
                    }
                });
            </script>
        </div>
    </div>
</div>
@endsection
