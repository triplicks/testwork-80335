<?php

namespace App\Http\Controllers;

use App\Report;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ReportController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
        $reports = Report::where('user_id', $id)->latest()->paginate(5);
        return view('reports.index', compact($reports))->withReports($reports)->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('reports.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'text' => 'required',
            'image' => 'required|max:10000|mimes:jpg,jpeg,png'
        ]);

        $data = $request->all();
        $file = $request->file('image');

        $data['image'] = $this->uploadImage($file);
        $data['user_id'] = Auth::id();

        Report::create($data);

        return response()->json(true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $report)
    {
        if (!$report->id) {
            return abort(404);
        }

        return view('reports.edit')->withReport($report);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $report = Report::where('user_id', Auth::id())->find($id);

        if (!$report) {
            return response()->json(['message' => 'not found'], 404);
        }

        $data = $request->all();
        $file = $request->file('image');

        if ($file) {
            $request->validate([
                'title' => 'required',
                'text' => 'required',
                'image' => 'required|max:10000|mimes:jpg,jpeg,png'
            ]);

            File::delete('img/'.$report->image);
            $data['image'] = $this->uploadImage($file);
        } else {
            $request->validate([
                'title' => 'required',
                'text' => 'required',
                'image' => 'required'
            ]);

            if ($request->image != $report->image) {
                return response()->json(['message' => 'Picture not transferred'], 404);
            }
        }

        Report::where('id', $id)->update($data);
        return response()->json(true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function destroy(Report $report)
    {
        if (!$report->id || $report->user_id != Auth::id()) {
            return response()->json(['message' => 'not found'], 404);
        }

        $report->delete();

        return response()->json(true);
    }

    /**
     * Upload image
     * @param Illuminate\Http\UploadedFile $image
     * @return string
     */
    private function uploadImage($image)
    {
        $name = date('YmdHis').$image->getClientOriginalName();
        $image->move('img', $name);

        return $name;
    }
}
